from Player import Player
from PresidentGame import PresidentGame

def configure_game():
    print("Bienvenu sur le jeu !")
    nb_players = int(input("Combien y aura-t-il de joueurs ?"))

    players = []
    for i in range(nb_players):
        players.append(Player(input("Nom du joueur {}: ".format(i + 1))))
    print("C'est parti on joue avec : {} !".format(players))

    return players

def main():
    # choice of the number of players and players names
    players = configure_game()
    game = PresidentGame(players)
    game.play()


main()
