import random

from Card import *


class Player:
    CHOICE_CARD = "1"
    CHOICE_PAIR = "2"
    CHOICE_THREE = "3"
    CHOICE_FOUR = "4"
    CHOICE_PASS = "5"
    def __init__(self, name=None):
        self.name = name or 'Joueurs {}'.format(random.randint(1, 1000))
        self.hand = []
        self.rank = "Joueur"
        self.lastPlayed = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def add_to_hand(self, cards):
        self.hand.append(cards)

    def remove_from_hand(self, cards):
        # Pose une carte
        for card in cards:
            self.hand.remove(card)

    def getPlayerPairs(self):
        pairs = []
        for card in self.hand:
            for other_card in self.hand:
                if card.equals_but_different_suit(other_card):
                    pairs.append([card, other_card])

                for other_card2 in self.hand:
                    if card.equals_but_different_suit(other_card, other_card2):
                        pairs.append([card, other_card, other_card2])

                    for other_card3 in self.hand:
                        if card.equals_but_different_suit(other_card, other_card2, other_card3):
                            pairs.append([card, other_card, other_card2, other_card3])

        pairs = [list(x) for x in set(tuple(x) for x in pairs)]
        # remove duplicates
        return pairs

    def playerTurn(self):
        print("========================================")
        print("========= C'est à ton tour, {}! ========".format(self.name))
        print("Votre main : {}".format(self.hand))
        print("Vos paires : {}".format(self.getPlayerPairs()))
        print("========================================")
