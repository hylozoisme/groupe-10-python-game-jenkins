import random

from Card import Card
from Deck import Deck
from Player import Player


class PresidentGame:
    """
        Règles :
            - Ce jeu se joue de 3 à 6 joueurs.
            - Lors du premier tour, un joueur aléatoire est défini pour commencer.
            - L'ensemble des cartes sont distribuées aux joueurs de la manière la plus homogène.
            - Ce jeu se joue par manches. Tant que quelqu'un peut et veut jouer,
              la manche continue et tourne dans le sens horaire.
            - Le premier joueur choisit des cartes d'une même valeur et les pose sur la tables.
            - Suite à celà, chaque joueur doit fournir autant de cartes que le joueur précédent
              des cartes dun' valeur supérieure ou égale.
            - Un joueur a le droit de sauter son tour et reprendre le tour d'après.
            - Un tour est fini lorsque plus personne ne joue. C'est alors le dernier
              à avoir joué qui ouvre la manche suivante.
            - L'objectif est d'être le premier à ne plus avoir de cartes. Ce joueur est alors déclaré président.
            - Les joueurs restants continuent à jouer jusqu'à ce qu'il n'y ait plus qu'une joueur
              qui ait des cartes en main, il est alors déclaré 'troufion
    """

    # 3 player by default
    def __init__(self, players=None):

        self.board = None
        self.players = players or [Player(), Player(), Player()]
        self.deck = Deck()
        self.deck.shuffle()
        self.distribute_cards()

    def distribute_cards(self):
        for player in self.players:
            player.hand = []

        for i, card in enumerate(self.deck.cards):
            self.players[i % len(self.players)].hand.append(card)

    def poser(self, player, cards):
        """
        :param player: the player who is playing
        :param cards: the cards the player wants to play
        :return: True if the player can play the cards, False otherwise
        """
        if self.board is None:
            self.board = cards
            player.hand.remove(cards)
            return True
        else:
            if self.board[0].value >= cards[0].value:
                self.board = cards
                player.remove_from_hand(cards)
                return True
            else:
                return False

    def play(self):
        current_player = random.choice(self.players)

        # while there is at least one player with cards
        while len([player for player in self.players if len(player.hand) > 0]) > 1:
            if len(current_player.hand) == 0:
                print("{} est le président !" . format(current_player))

            current_player.playerTurn()

            choice = input("Quelle action voulez-vous faire ? (1: Poser une carte, 2: Poser une paire, 3: Poser un "
                           "triple, 4: Poser un quadruple, 5: Passer) ")

            while choice not in ['1', '2', '3', '4', '5']:
                choice = input("Quelle action voulez-vous faire ? (1: Poser une carte, 2: Poser une paire, 3: Poser un "
                               "triple, 4: Poser un quadruple, 5: Passer) ")


            if choice == Player.CHOICE_CARD:
                card = input("Quelle carte voulez-vous poser ? ")

                while card not in current_player.hand:
                    card = input("Quelle carte voulez-vous poser ? ")

                if self.poser(current_player, card):
                    print("Vous avez posé {}" . format(card))
                else:
                    print("Vous ne pouvez pas poser cette carte !")

            elif choice == Player.CHOICE_PAIR:
                inputCards = input("Quelle paire voulez-vous poser ? ")
                pair = Card.make_a_pair_from_input(inputCards)

                while pair not in current_player.hand:
                    pair = input("Quelle paire voulez-vous poser ? ")

                if self.poser(current_player, pair):
                    print("Vous avez posé {}" . format(pair))
                else:
                    print("Vous ne pouvez pas poser cette paire !")

            elif choice == Player.CHOICE_PASS:
                print("Vous passez votre tour !")

    def __str__(self):
        return 'Game is started with {} players !' . format(len(self.players))
