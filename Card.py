class Card:
    names = {'3': 1, '4': 2, '5': 3, '6': 4, '7': 5, '8': 6, '9': 7, '10': 8, 'J': 9, 'Q': 10, 'K': 11, 'A': 12, '2': 13}
    suits = ["♦️", "♥️", "♠️", "♣️"]

    def __init__(self, name=None, suit=None):
        self.suit = suit
        self.name = name

    def __str__(self):
        return self.name + self.suit

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.names[self.name] == self.names[other.name]

    def __lt__(self, other):
        return self.names[self.name] < self.names[other.name]

    def __gt__(self, other):
        return self.names[self.name] > self.names[other.name]

    def __hash__(self):
        return hash(self.name)

    def make_a_card_from_input(self, input):
        return Card(input[0], input[1])

    def make_a_pair_from_input(self, input):
        print(input)
        return [Card(input[0], input[1]), Card(input[3], input[4])]

    def equals_but_different_suit(self, other, other2=None, other3=None):
        if other3 is not None:
            return self.names[self.name] == self.names[other.name] == self.names[other2.name] == self.names[other3.name] and self.suit != other.suit and self.suit != other2.suit and self.suit != other3.suit and other.suit != other2.suit and other.suit != other3.suit and other2.suit != other3.suit
        elif other2 is not None:
            return self.names[self.name] == self.names[other.name] == self.names[other2.name] and self.suit != other.suit and self.suit != other2.suit and other.suit != other2.suit
        else:
            return self.names[self.name] == self.names[other.name] and self.suit != other.suit

